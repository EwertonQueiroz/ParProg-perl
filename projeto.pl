#!/usr/bin/env perl

use strict;
use warnings;
use Time::HiRes qw/gettimeofday/;

if (scalar @ARGV != 3) {
	print "Informe 3 parametros\n";
	exit(1);
} 

foreach my $argNum (0 .. $#ARGV) {
	my $escrita;
	my $tempoInicio = int(gettimeofday() * 1000);
	#print "Tempo inicial:\t $tempoInicio\n";
	
	my $local;
	my @linhaTratada;
	my $count;
	my $resultComp;

	my $quatroIguais = 0;
	my $todosDiferentes = 0;
	my $sequencia = 0;
	my $nada = 0;
	
	open(my $file, '<'.$ARGV[$argNum]) || die "Nao foi possivel abrir o arquivo '$ARGV[$argNum]':\n $!";
	
	#=pod
	while (!eof($file)) {
		# Pega a posição do ponteiro de leitura do arquivo
		$local = tell($file);
		
		# Move o ponteiro de leitura para a posição onde ele parou, contando
		# a partir do início do arquivo
		seek($file, $local, 0);
		
		# Lê a linha, movendo assim o ponteiro de leitura para o fim da mesma
		my $linha = <$file>;
		
		$count = 0;
		$resultComp = 0;
		
		# Remove espaços em branco da linha lida
		@linhaTratada = split(/ /, $linha);
		#print "\nLinha pos split:  @linhaTratada\t->     ";
		
		# Para cada caractere da linha pós split, trate a mesma substituindo as letras por valores
		for (my $x = 0; $x < scalar @linhaTratada; $x++) {
			# Atribui valor a cada letra
			if ($linhaTratada[$x] eq 'T' || $linhaTratada[$x] eq "T\r\n") {
				$linhaTratada[$x] = 10;
			}
			
			if ($linhaTratada[$x] eq 'J' || $linhaTratada[$x] eq "J\r\n") {
				$linhaTratada[$x] = 11;
			}
			
			if ($linhaTratada[$x] eq 'Q' || $linhaTratada[$x] eq "Q\r\n") {
				$linhaTratada[$x] = 12;
			}
			
			if ($linhaTratada[$x] eq 'K' || $linhaTratada[$x] eq "K\r\n") {
				$linhaTratada[$x] = 13;
			}
			
			if ($linhaTratada[$x] eq 'A' || $linhaTratada[$x] eq "A\r\n") {
				$linhaTratada[$x] = 14;
			}

			# Imprima o resultado da linha tratada elemento por elemento
			#print "'$linhaTratada[$x]'";
		}
		
		# Imprime a lista contendo a linha tratada
		#print "\n@linhaTratada\n";
		
		# Ordene a linha já tratada e imprima
		@linhaTratada = sort { $a <=> $b } @linhaTratada;
		#print "\nLinha pos sort:\t\t\t       '@linhaTratada'\n";
		
		# Inicio das buscas por padrões
		
		# Quatro iguais
		for (my $i = 0; $i < scalar @linhaTratada; $i++) {
			for (my $j = $i + 1; $j < scalar @linhaTratada; $j++) {
				if ($linhaTratada[$i] == $linhaTratada[$j]) {
					$count++;
				}
			}
		}
		
		# Todos serão iguais quando a contagem der 6
		if ($count == 6) {
			$quatroIguais++;
		}
		
		# Todos serão diferentes quando a contagem der 0, uma vez que todos são diferentes
		# devemos analisar se existe sequência
		if ($count == 0) {
			# Sequências
			for (my $i = 0; $i < scalar @linhaTratada; $i++) {
				if ($i < scalar @linhaTratada - 1 && $linhaTratada[$i] + 1 == $linhaTratada[$i + 1]) {
					$resultComp++;
				}
			}
			
			# Se o resultado das comparações der 4, existe sequência
			if ($resultComp == 4) {
				$sequencia++;
			}
			
			# Se não, todos são diferentes
			else {
				$todosDiferentes++;
			}
		}
		
		# Se o contador estiver entre 1 e 5, é nada
		elsif ($count > 0 && $count < 6) {
			$nada++;
		}
	
		#print "Resultado Comparacao: $resultComp\n";
		#print "Contador: $count\n";
	}
	
	my $tempoTotal = int(gettimeofday() * 1000);
	#print "Tempo final:\t $tempoTotal\n";
	
	$tempoTotal -= $tempoInicio;
	
	#=cut
	#print "Tempo total:\t $tempoTotal\n";
	#print "Quatro iguais: $quatroIguais\n";
	#print "Todos diferentes: $todosDiferentes\n";
	#print "Sequencias: $sequencia\n";
	#print "Nada: $nada\n";
	
	# Fecha o arquivo lido
	close($file) || die "Erro ao fechar o arquivo $ARGV[$argNum]:\n $!";
	
	# Verifica se é o primeiro parametro, se for, abre o arquivo para escrita no modo Write
	if ($argNum == 0) {
		open($escrita, '>saida.txt') || die "Nao foi possivel criar o arquivo 'saida.txt':\n $!";
	}
	
	# Se não, abre no modo Append
	else {
		open($escrita, '>>saida.txt') || die "Nao foi possivel criar o arquivo 'saida.txt':\n $!";
	}
	
	print $escrita "$tempoTotal $quatroIguais $todosDiferentes $sequencia\n";
	
	close($escrita) || die "Erro ao fechar o arquivo 'saida.txt':\n $!";
}

exit(0);